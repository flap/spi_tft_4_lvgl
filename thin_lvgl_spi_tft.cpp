#include <Arduino.h>
#include <SPI.h>
#include "thin_lvgl_spi_tft.h"

/** bottom of the display is where the connectors are */
#define DISPLAY_PORTRAIT (TFT_MAD_MX | TFT_MAD_MY | TFT_MAD_COLOR_ORDER)
/** Top of the display is where the connectors are */
#define DISPLAY_PORTRAIT_UPSIDE_DOWN (TFT_MAD_MY | TFT_MAD_MV | TFT_MAD_COLOR_ORDER)
/** Top of the display is when the connectors are at the right side */
#define DISPLAY_LANDSCAPE (TFT_MAD_COLOR_ORDER)
/** Top of the display is when the connectors are at the left side */
#define DISPLAY_LANDSCAPE_MIRROR (TFT_MAD_MX | TFT_MAD_MV | TFT_MAD_COLOR_ORDER)

/**
 * Define the required SPI settings to communicate with the display controller
 *
 * @note For this small display it seems a slowe SPI speed results into smoother
 *       animations than a faster SPI speed.
 */
//#define DISPLAY_SPI_SETTING (SPISettings(4000000, MSBFIRST, SPI_MODE0))
#define DISPLAY_SPI_SETTING (SPISettings(27000000, MSBFIRST, SPI_MODE0))

#include "ST7735_Defines.h"

thin_lvgl_spi_tft::thin_lvgl_spi_tft(void)
{
}

inline void thin_lvgl_spi_tft::assert_reset(void)
{
	digitalWrite(TFT_CHIP_RESET_LINE, LOW);
}

inline void thin_lvgl_spi_tft::release_reset(void)
{
	digitalWrite(TFT_CHIP_RESET_LINE, HIGH);
}

inline void thin_lvgl_spi_tft::notify_command(void)
{
	digitalWrite(TFT_CHIP_DATACOMMAND_LINE, LOW);
}

inline void thin_lvgl_spi_tft::notify_data(void)
{
	digitalWrite(TFT_CHIP_DATACOMMAND_LINE, HIGH);
}

inline void thin_lvgl_spi_tft::assert_select(void)
{
	digitalWrite(TFT_CHIP_CS_LINE, LOW);
}

inline void thin_lvgl_spi_tft::release_select(void)
{
	digitalWrite(TFT_CHIP_CS_LINE, HIGH);
}

void thin_lvgl_spi_tft::io_setup(void)
{
	pinMode(TFT_CHIP_RESET_LINE, OUTPUT);
	pinMode(TFT_CHIP_DATACOMMAND_LINE, OUTPUT);
	pinMode(TFT_CHIP_CS_LINE, OUTPUT);

	release_select();
	assert_reset();
	notify_data();

	SPI.begin();
}

void thin_lvgl_spi_tft::command_write(unsigned char c)
{
	notify_command();
	SPI.beginTransaction(DISPLAY_SPI_SETTING);
	assert_select();
	SPI.transfer(c);
	release_select();
	SPI.endTransaction();
}

void thin_lvgl_spi_tft::data_write(unsigned char c)
{
	notify_data();
	SPI.beginTransaction(DISPLAY_SPI_SETTING);
	assert_select();
	SPI.transfer(c);
	release_select();
	SPI.endTransaction();
}

void thin_lvgl_spi_tft::commandList(const uint8_t *addr)
{
	uint8_t  numCommands;
	uint8_t  numArgs;
	uint8_t  ms;

	numCommands = pgm_read_byte(addr++);   // Number of commands to follow

	while (numCommands--) {                  // For each command...
		command_write(pgm_read_byte(addr++)); // Read, issue command
		numArgs = pgm_read_byte(addr++);     // Number of args to follow
		ms = numArgs & TFT_INIT_DELAY;       // If hibit set, delay follows args
		numArgs &= ~TFT_INIT_DELAY;          // Mask out delay bit

		while (numArgs--)                    // For each argument...
			data_write(pgm_read_byte(addr++));  // Read, issue argument

		if (ms) {
			ms = pgm_read_byte(addr++);        // Read post-command delay time (ms)
			delay( (ms==255 ? 500 : ms) );
		}
	}
}

// Initialization commands for ST7735 screens
static __attribute__((unused)) const uint8_t PROGMEM Bcmd[] = {
    18,                       // 18 commands in list:
    ST7735_SWRESET,   TFT_INIT_DELAY,  //  1: Software reset, no args, w/delay
      50,                     //     50 ms delay
    ST7735_SLPOUT ,   TFT_INIT_DELAY,  //  2: Out of sleep mode, no args, w/delay
      255,                    //     255 = 500 ms delay
    ST7735_COLMOD , 1 + TFT_INIT_DELAY,  //  3: Set color mode, 1 arg + delay:
      0x05,                   //     16-bit color <--- das würde die LUT benutzen???
      10,                     //     10 ms delay
    ST7735_FRMCTR1, 3 + TFT_INIT_DELAY,  //  4: Frame rate control, 3 args + delay:
      0x00,                   //     fastest refresh
      0x06,                   //     6 lines front porch
      0x03,                   //     3 lines back porch
      10,                     //     10 ms delay
    ST7735_MADCTL , 1      ,  //  5: Memory access ctrl (directions), 1 arg:
      DISPLAY_PORTRAIT,       //     Row addr/col addr, bottom to top refresh
    ST7735_DISSET5, 2      ,  //  6: Display settings #5, 2 args, no delay:
      0x15,                   //     1 clk cycle nonoverlap, 2 cycle gate
                              //     rise, 3 cycle osc equalize
      0x02,                   //     Fix on VTL
    ST7735_INVCTR , 1      ,  //  7: Display inversion control, 1 arg:
      0x0,                    //     Line inversion
    ST7735_PWCTR1 , 2 + TFT_INIT_DELAY,  //  8: Power control, 2 args + delay:
      0x02,                   //     GVDD = 4.7V
      0x70,                   //     1.0uA
      10,                     //     10 ms delay
    ST7735_PWCTR2 , 1      ,  //  9: Power control, 1 arg, no delay:
      0x05,                   //     VGH = 14.7V, VGL = -7.35V
    ST7735_PWCTR3 , 2      ,  // 10: Power control, 2 args, no delay:
      0x01,                   //     Opamp current small
      0x02,                   //     Boost frequency
    ST7735_VMCTR1 , 2+TFT_INIT_DELAY,  // 11: Power control, 2 args + delay:
      0x3C,                   //     VCOMH = 4V
      0x38,                   //     VCOML = -1.1V
      10,                     //     10 ms delay
    ST7735_PWCTR6 , 2      ,  // 12: Power control, 2 args, no delay:
      0x11, 0x15,
    ST7735_GMCTRP1,16      ,  // 13: Magical unicorn dust, 16 args, no delay:
      0x09, 0x16, 0x09, 0x20, //     (seriously though, not sure what
      0x21, 0x1B, 0x13, 0x19, //      these config values represent)
      0x17, 0x15, 0x1E, 0x2B,
      0x04, 0x05, 0x02, 0x0E,
    ST7735_GMCTRN1,16+TFT_INIT_DELAY,  // 14: Sparkles and rainbows, 16 args + delay:
      0x0B, 0x14, 0x08, 0x1E, //     (ditto)
      0x22, 0x1D, 0x18, 0x1E,
      0x1B, 0x1A, 0x24, 0x2B,
      0x06, 0x06, 0x02, 0x0F,
      10,                     //     10 ms delay
    ST7735_CASET  , 4      ,  // 15: Column addr set, 4 args, no delay:
      0x00, 0x02,             //     XSTART = 2
      0x00, 0x81,             //     XEND = 129
    ST7735_RASET  , 4      ,  // 16: Row addr set, 4 args, no delay:
      0x00, 0x02,             //     XSTART = 1
      0x00, 0x81,             //     XEND = 160
    ST7735_NORON  ,   TFT_INIT_DELAY,  // 17: Normal display on, no args, w/delay
      10,                     //     10 ms delay
    ST7735_DISPON ,   TFT_INIT_DELAY,  // 18: Main screen turn on, no args, w/delay
      255,                    //     255 = 500 ms delay
};

// Init for 7735R, part 1 (red or green tab)
static __attribute__((unused)) const uint8_t PROGMEM Rcmd1[] = {
    15,                       // 15 commands in list:
    ST7735_SWRESET,   TFT_INIT_DELAY,  //  1: Software reset, 0 args, w/delay
      150,                    //     150 ms delay
    ST7735_SLPOUT ,   TFT_INIT_DELAY,  //  2: Out of sleep mode, 0 args, w/delay
      255,                    //     500 ms delay
    ST7735_FRMCTR1, 3      ,  //  3: Frame rate ctrl - normal mode, 3 args:
      0x01, 0x2C, 0x2D,       //     Rate = fosc/(1x2+40) * (LINE+2C+2D)
    ST7735_FRMCTR2, 3      ,  //  4: Frame rate control - idle mode, 3 args:
      0x01, 0x2C, 0x2D,       //     Rate = fosc/(1x2+40) * (LINE+2C+2D)
    ST7735_FRMCTR3, 6      ,  //  5: Frame rate ctrl - partial mode, 6 args:
      0x01, 0x2C, 0x2D,       //     Dot inversion mode
      0x01, 0x2C, 0x2D,       //     Line inversion mode
    ST7735_INVCTR , 1      ,  //  6: Display inversion ctrl, 1 arg, no delay:
      0x07,                   //     No inversion
    ST7735_PWCTR1 , 3      ,  //  7: Power control, 3 args, no delay:
      0xA2,
      0x02,                   //     -4.6V
      0x84,                   //     AUTO mode
    ST7735_PWCTR2 , 1      ,  //  8: Power control, 1 arg, no delay:
      0xC5,                   //     VGH25 = 2.4C VGSEL = -10 VGH = 3 * AVDD
    ST7735_PWCTR3 , 2      ,  //  9: Power control, 2 args, no delay:
      0x0A,                   //     Opamp current small
      0x00,                   //     Boost frequency
    ST7735_PWCTR4 , 2      ,  // 10: Power control, 2 args, no delay:
      0x8A,                   //     BCLK/2, Opamp current small & Medium low
      0x2A,
    ST7735_PWCTR5 , 2      ,  // 11: Power control, 2 args, no delay:
      0x8A, 0xEE,
    ST7735_VMCTR1 , 1      ,  // 12: Power control, 1 arg, no delay:
      0x0E,
    ST7735_INVOFF , 0      ,  // 13: Don't invert display, no args, no delay
    ST7735_MADCTL , 1      ,  // 14: Memory access control (directions), 1 arg:
      DISPLAY_PORTRAIT,       //     row addr/col addr, bottom to top refresh
    ST7735_COLMOD , 1      ,  // 15: set color mode, 1 arg, no delay:
      0x05,                   //     16-bit color <--- das würde die LUT benutzen und kein RGB888!!!!
};

static __attribute__((unused)) const uint8_t PROGMEM Rcmd2green[] = {            // Init for 7735R, part 2 (green tab only)
    2,                        //  2 commands in list:
    ST7735_CASET  , 4      ,  //  1: Column addr set, 4 args, no delay:
      0x00, 0x02,             //     XSTART = 0
      0x00, 0x7F+0x02,        //     XEND = 127
    ST7735_RASET  , 4      ,  //  2: Row addr set, 4 args, no delay:
      0x00, 0x01,             //     XSTART = 0
      0x00, 0x9F+0x01,        //     XEND = 159
};

static __attribute__((unused)) const uint8_t PROGMEM Rcmd2red[] = {              // Init for 7735R, part 2 (red tab only)
    2,                        //  2 commands in list:
    ST7735_CASET  , 4      ,  //  1: Column addr set, 4 args, no delay:
      0x00, 0x00,             //     XSTART = 0
      0x00, 0x7F,             //     XEND = 127
    ST7735_RASET  , 4      ,  //  2: Row addr set, 4 args, no delay:
      0x00, 0x00,             //     XSTART = 0
      0x00, 0x9F,             //     XEND = 159
};

static __attribute__((unused)) const uint8_t PROGMEM Rcmd3[] = {                 // Init for 7735R, part 3 (red or green tab)
    3,                        //  3 commands in list:
    ST7735_GMCTRP1, 16      , //  1: 16 args, no delay:
      0x02, 0x1c, 0x07, 0x12,
      0x37, 0x32, 0x29, 0x2d,
      0x29, 0x25, 0x2B, 0x39,
      0x00, 0x01, 0x03, 0x10,
    ST7735_GMCTRN1, 16      , //  2: 16 args, no delay:
      0x03, 0x1d, 0x07, 0x06,
      0x2E, 0x2C, 0x29, 0x2D,
      0x2E, 0x2E, 0x37, 0x3F,
      0x00, 0x00, 0x02, 0x10,
    ST7735_NORON  ,    TFT_INIT_DELAY, //  3: Normal display on, no args, w/delay
      10,                     //     10 ms delay
};

#if (TFT_CHIP_TYPE == TFT_CHIP_TYPE_ST7735)
/** simple LUT for the two 5 bit red and blue colours */
static const uint8_t rblut_lower[8] = {
	0x00, // 0x00
	0x01, // 0x01
	0x03, // 0x02
	0x05, // 0x03
	0x08, // 0x04
	0x0A, // 0x05
	0x0C, // 0x06
	0x0E, // 0x07
};

static void lut_write(const uint8_t *lut, unsigned size)
{
	while (size > 0) {
		SPI.transfer(*lut++);
		size--;
	}
}

/** Init a 7735 based TFT to RGB565 pixel format */
void thin_lvgl_spi_tft::init_7735x_chips(void)
{
#if (TFT_DISPLAY_TYPE == WITH_REDTAB)
	commandList(Rcmd1);
	commandList(Rcmd2red);
	commandList(Rcmd3);

	notify_command();
	assert_select();
	SPI.beginTransaction(DISPLAY_SPI_SETTING);
	SPI.transfer(ST7735_RGBSET);

	notify_data();
	// Special LUT for RED (5 bit to 6 bit expansion)
	lut_write(rblut_lower, 8);		// non-linear part
	for (unsigned u = 8; u < 32; u++)	// linerar part
	     SPI.transfer(u * 2);
	// Generic LUT for GREEN (1:1)
	for (unsigned u = 0; u < 64; u++)	// linerar part
	     SPI.transfer(u);
	// Special LUT for BLUE (5 bit to 6 bit expansion)
	lut_write(rblut_lower, 8);		// non-linear part
	for (unsigned u = 8; u < 32; u++)	// linerar part
	     SPI.transfer(u * 2);

	release_select();
	SPI.endTransaction();
#else
# error "No 7735x based TFT display selected"
#endif
}
#endif

void thin_lvgl_spi_tft::display_init(void)
{
	release_select();
	release_reset();
	delay(5);
	assert_reset();
	delay(20);
	release_reset();
	delay(150);	// Wait for the device to complete the reset

#if (TFT_CHIP_TYPE == TFT_CHIP_TYPE_ST7735)
	init_7735x_chips();
#endif
}

void thin_lvgl_spi_tft::display_enable(void)
{
	command_write(ST7735_DISPON);
}

void thin_lvgl_spi_tft::display_disable(void)
{
	command_write(ST7735_DISPOFF);
}

/**
 * Blit a source image with linear data to a rectangle area on the display
 * @param[in] buffer Where to read from. Points to an array of #lv_color16_t
 * @param[in] x_start Destination start X coordinate
 * @param[in] y_start Destination start Y coordinate
 * @param[in] x_end Destination end X coordinate
 * @param[in] y_end Destination end Y coordinate
 *
 * To able to sent the pixel colour info whithout any further processing, the
 * following data organisation in memory is required:
 *
 *  Byte#0      Byte#1
 *----------------------
 * 7654 3210   7654 3210
 *----------------------
 * gggb bbbb   rrrr rggg
 *
 * When reading in on a litte endian machine this layout results into:
 *
 *  1111 1100 0000 0000
 *  5432 1098 7654 3210
 * ----------------------
 *  rrrr rggg gggb bbbb
 *
 * When sent with the MSB first it is exactly the data format the display
 * controller expects.
 *
 * This specific layout can be maintained by a compiler with the following
 * structure:
 *       {
 *        uint16_t blue:5;
 *        uint16_t green:6;
 *        uint16_t red:5;
 *       }
 *
 * In LVGL you can use such kind of structure by defining LV_COLOR_16_SWAP to 0
 */
void thin_lvgl_spi_tft::area_flush(const void *buffer, int x_start, int y_start, int x_end, int y_end)
{
	uint16_t x0 = x_start, y0 = y_start, x1 = x_end, y1 = y_end;
	const uint16_t *data = (const uint16_t *)buffer;
	int count = (x_end - x_start + 1) * (y_end - y_start + 1);

	if (x_start > x_end)
		return; // due to x0 <= x1
	if (y_start > y_end)
		return; // due to y0 <= y1

	// Send all data with MSB first
	SPI.beginTransaction(DISPLAY_SPI_SETTING);
	notify_command();
	assert_select();
	SPI.transfer(TFT_CASET); // Collumn Address Set (2 shorts in BE as parameters)
	notify_data();
	SPI.transfer16(x0);	// XS[15:0] in BE
	SPI.transfer16(x1);	// XE[15:0] in BE
	notify_command();
	SPI.transfer(TFT_PASET); // Row Address Set (2 shorts in BE as parameters)
	notify_data();
	SPI.transfer16(y0);	// YS[15:0] in BE
	SPI.transfer16(y1);	// YE[15:0] in BE

	notify_command();
	SPI.transfer(TFT_RAMWR); // Start a burst write of data
	notify_data();
	while (count--) {
		SPI.transfer16(*data); // One pixel in BE per pixel
		data++;
	}

	release_select();
	SPI.endTransaction();
}
