Very thin SPI based TFT library for lvgl
========================================

It contains nothing more than required to make LVGL work. Derived from TFT_eSPI.

Since TFT_eSPI comes with its own drawing primitives and LVGL adds another level
of drawing on top of it, the drawing support in TFT_eSPI is useless and just
consumes ROM and RAM.

This driver variant is for the "red tap" variant of the 128 x 160 1.8" TFT
display. It supports the "16 bit per pixel" LVGL setting and compile time
rotation only.

Note: this variant is tested only on an ARM based machine. Since some data is
      located in the ROM it might not work on Atmega-MCUs (due to their
      Harvard architecture) without adaptions. Patches are welcome.
