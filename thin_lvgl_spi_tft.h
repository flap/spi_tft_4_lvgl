
#define TFT_CHIP_TYPE_ST7735 0
#define TFT_CHIP_TYPE_ST7735B 1
#define TFT_CHIP_TYPE_ST7735R 2

#define WITH_GREENTAB 0
#define WITH_REDTAB 1

#ifndef TFT_WIDTH
# define TFT_WIDTH 128
#endif

#ifndef TFT_HEIGHT
# define TFT_HEIGHT 160
#endif

#ifndef TFT_CHIP_TYPE
# define TFT_CHIP_TYPE TFT_CHIP_TYPE_ST7735
#endif

#ifndef TFT_DISPLAY_TYPE
# define TFT_DISPLAY_TYPE WITH_REDTAB
#endif

#ifndef TFT_CHIP_RESET_LINE
# define TFT_CHIP_RESET_LINE 7
#endif

#ifndef TFT_CHIP_DATACOMMAND_LINE
# define TFT_CHIP_DATACOMMAND_LINE 8
#endif

#ifndef TFT_CHIP_CS_LINE
# define TFT_CHIP_CS_LINE 10
#endif

/* Type of coordinates. Should be the same like lv_coord_t from the lv_conf.h */
typedef int16_t lvgl_coord_t;

class thin_lvgl_spi_tft {
public:
	thin_lvgl_spi_tft(void);
	void io_setup(void);
	void display_init(void); /**< Init the controller */
	void display_enable(void); /**< Enable the TFT glass */
	void display_disable(void); /**< Disable the TFT glass */
	void area_flush(const void *buffer, int x_start, int y_start, int x_end, int y_end);

private:
	void inline assert_reset(void);
	void inline release_reset(void);
	void inline notify_command(void);
	void inline notify_data(void);
	void inline assert_select(void);
	void inline release_select(void);

	void command_write(unsigned char c);
	void data_write(unsigned char d);
	void commandList (const uint8_t *addr);

	void init_7735x_chips(void);
};
